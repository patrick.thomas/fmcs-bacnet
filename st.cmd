#!../../bin/linux-x86/epics2bacnet

## You may have to change epics2bacnet to something else
## everywhere it appears in this file

< envPaths

cd ${TOP}

## Register all support components
dbLoadDatabase "dbd/epics2bacnet.dbd"
epics2bacnet_registerRecordDeviceDriver pdbbase

# You have to provide a Hardware Interface Name that matches the
# name of your hardware... If you don't "eth0" is default...
bacnetDriverInterfaceName("eth1")

#Default and most common BACnet port number...
#"47808" is default...
bacnetDriverPortNumber(47808)


#Define a bacnet Server, Provide a name etc...
#bacnetDefineServer("<someName>", <BACnet instance number>, <BACnet port>, <poll_delay in milliseconds>

bacnetDefineServer("bacnet11000", 11000, 47808, 25)
bacnetDefineServer("bacnet11010", 11010, 47808, 25)
bacnetDefineServer("bacnet11011", 11011, 47808, 25)
bacnetDefineServer("bacnet11012", 11012, 47808, 25)
bacnetDefineServer("bacnet11013", 11013, 47808, 25)
bacnetDefineServer("bacnet11014", 11014, 47808, 25)
bacnetDefineServer("bacnet11015", 11015, 47808, 25)
bacnetDefineServer("bacnet11016", 11016, 47808, 25)
bacnetDefineServer("bacnet11017", 11017, 47808, 25)
bacnetDefineServer("bacnet11018", 11018, 47808, 25)
bacnetDefineServer("bacnet11019", 11019, 47808, 25)
bacnetDefineServer("bacnet11020", 11020, 47808, 25)
bacnetDefineServer("bacnet11021", 11021, 47808, 25)
bacnetDefineServer("bacnet11035", 11035, 47808, 25)
bacnetDefineServer("bacnet11036", 11036, 47808, 25)
bacnetDefineServer("bacnet11037", 11037, 47808, 25)
bacnetDefineServer("bacnet11038", 11038, 47808, 25)
bacnetDefineServer("bacnet11039", 11039, 47808, 25)
bacnetDefineServer("bacnet11040", 11040, 47808, 25)
bacnetDefineServer("bacnet11070", 11070, 47808, 25)
bacnetDefineServer("bacnet12010", 12010, 47808, 25)
bacnetDefineServer("bacnet12015", 12015, 47808, 25)
bacnetDefineServer("bacnet12071", 12071, 47808, 25)
bacnetDefineServer("bacnet12073", 12073, 47808, 25)
bacnetDefineServer("bacnet12075", 12075, 47808, 25)
bacnetDefineServer("bacnet13001", 13001, 47808, 25)
bacnetDefineServer("bacnet13002", 13002, 47808, 25)
bacnetDefineServer("bacnet13003", 13003, 47808, 25)
bacnetDefineServer("bacnet13012", 13012, 47808, 25)
bacnetDefineServer("bacnet14001", 14001, 47808, 25)
bacnetDefineServer("bacnet14002", 14002, 47808, 25)
bacnetDefineServer("bacnet14003", 14003, 47808, 25)
bacnetDefineServer("bacnet14012", 14012, 47808, 25)
bacnetDefineServer("bacnet15001", 15001, 47808, 25)
bacnetDefineServer("bacnet15002", 15002, 47808, 25)
bacnetDefineServer("bacnet15003", 15003, 47808, 25)
bacnetDefineServer("bacnet15012", 15012, 47808, 25)
bacnetDefineServer("bacnet16001", 16001, 47808, 25)
bacnetDefineServer("bacnet16002", 16002, 47808, 25)
bacnetDefineServer("bacnet16003", 16003, 47808, 25)
bacnetDefineServer("bacnet16011", 16011, 47808, 25)
bacnetDefineServer("bacnet16012", 16012, 47808, 25)
bacnetDefineServer("bacnet19001", 19001, 47808, 25)
bacnetDefineServer("bacnet19002", 19002, 47808, 25)
bacnetDefineServer("bacnet19003", 19003, 47808, 25)
bacnetDefineServer("bacnet19004", 19004, 47808, 25)
bacnetDefineServer("bacnet19065", 19065, 47808, 25)


## Load record instances
dbLoadRecords "db/fmcs_bacnet.db", "IFO=H0"
dbLoadRecords "db/chiller_bacnet.db", "IFO=H0"
dbLoadRecords "db/fces_bacnet.db", "IFO=H0"

## Set this to see messages from mySub
#var mySubDebug 1

## Run this to trace the stages of iocInit
#traceIocInit

cd ${TOP}/iocBoot/${IOC}
iocInit

bacnetListServerInfo(5)

bacnetDriverStart(1)

bacnetDriverVerbosity(1)

## Start any sequence programs
#seq sncExample, "user=8w4Host"
