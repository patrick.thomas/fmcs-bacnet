#!../../bin/linux-x86_64/bacnet

## You may have to change bacnet to something else
## everywhere it appears in this file.

< envPaths

cd ${TOP}

## Register all support components
dbLoadDatabase "dbd/bacnet.dbd"
bacnet_registerRecordDeviceDriver pdbbase

# /** Define BACnet Stuff... */

# /** Define a BACnet Device... (<name>, <device-instance>, nic_name, <[port]>)*/
# i.p. addr for  160.91.233.141 "ALC_ME812UE"
#drvBACnetDefineDevice("ALC_ME812UE", 22, "ens33")
#drvBACnetDefineDevice("ALC_SE6104A", 21, "ens33", 47808)
#drvBACnetDefineDevice("FieldServer", 11, "eth0")

drvBACnetDefineDevice("bacnet11000", 11000, "eth1", 47808)
drvBACnetDefineDevice("bacnet11010", 11010, "eth1", 47808)
drvBACnetDefineDevice("bacnet11011", 11011, "eth1", 47808)
drvBACnetDefineDevice("bacnet11012", 11012, "eth1", 47808)
drvBACnetDefineDevice("bacnet11013", 11013, "eth1", 47808)
drvBACnetDefineDevice("bacnet11014", 11014, "eth1", 47808)
drvBACnetDefineDevice("bacnet11015", 11015, "eth1", 47808)
drvBACnetDefineDevice("bacnet11016", 11016, "eth1", 47808)
drvBACnetDefineDevice("bacnet11017", 11017, "eth1", 47808)
drvBACnetDefineDevice("bacnet11018", 11018, "eth1", 47808)
drvBACnetDefineDevice("bacnet11019", 11019, "eth1", 47808)
drvBACnetDefineDevice("bacnet11020", 11020, "eth1", 47808)
drvBACnetDefineDevice("bacnet11021", 11021, "eth1", 47808)
drvBACnetDefineDevice("bacnet11035", 11035, "eth1", 47808)
drvBACnetDefineDevice("bacnet11036", 11036, "eth1", 47808)
drvBACnetDefineDevice("bacnet11037", 11037, "eth1", 47808)
drvBACnetDefineDevice("bacnet11038", 11038, "eth1", 47808)
drvBACnetDefineDevice("bacnet11039", 11039, "eth1", 47808)
drvBACnetDefineDevice("bacnet11040", 11040, "eth1", 47808)
drvBACnetDefineDevice("bacnet11070", 11070, "eth1", 47808)
drvBACnetDefineDevice("bacnet12010", 12010, "eth1", 47808)
drvBACnetDefineDevice("bacnet12015", 12015, "eth1", 47808)
drvBACnetDefineDevice("bacnet12071", 12071, "eth1", 47808)
drvBACnetDefineDevice("bacnet12073", 12073, "eth1", 47808)
drvBACnetDefineDevice("bacnet12075", 12075, "eth1", 47808)
drvBACnetDefineDevice("bacnet13001", 13001, "eth1", 47808)
drvBACnetDefineDevice("bacnet13002", 13002, "eth1", 47808)
drvBACnetDefineDevice("bacnet13003", 13003, "eth1", 47808)
drvBACnetDefineDevice("bacnet13012", 13012, "eth1", 47808)
drvBACnetDefineDevice("bacnet14001", 14001, "eth1", 47808)
drvBACnetDefineDevice("bacnet14002", 14002, "eth1", 47808)
drvBACnetDefineDevice("bacnet14003", 14003, "eth1", 47808)
drvBACnetDefineDevice("bacnet14012", 14012, "eth1", 47808)
drvBACnetDefineDevice("bacnet15001", 15001, "eth1", 47808)
drvBACnetDefineDevice("bacnet15002", 15002, "eth1", 47808)
drvBACnetDefineDevice("bacnet15003", 15003, "eth1", 47808)
drvBACnetDefineDevice("bacnet15012", 15012, "eth1", 47808)
drvBACnetDefineDevice("bacnet16001", 16001, "eth1", 47808)
drvBACnetDefineDevice("bacnet16002", 16002, "eth1", 47808)
drvBACnetDefineDevice("bacnet16003", 16003, "eth1", 47808)
drvBACnetDefineDevice("bacnet16011", 16011, "eth1", 47808)
drvBACnetDefineDevice("bacnet16012", 16012, "eth1", 47808)
drvBACnetDefineDevice("bacnet19001", 19001, "eth1", 47808)
drvBACnetDefineDevice("bacnet19002", 19002, "eth1", 47808)
drvBACnetDefineDevice("bacnet19003", 19003, "eth1", 47808)
drvBACnetDefineDevice("bacnet19004", 19004, "eth1", 47808)
drvBACnetDefineDevice("bacnet19065", 19065, "eth1", 47808)

#drvBACnetSet("ALC_SE6104A", "apdu_timeout=3000, apdu_retries=3")
#drvBACnetSet("FieldServer", "rpm_buffer_limit=500, rpm_disable=false")

## Load record instances
#dbLoadRecords "db/Test_1.db"
dbLoadRecords "db/monitor.db", "IFO=H0"
dbLoadRecords "db/fmcs_bacnet_bi_to_ai.db", "IFO=H0"
dbLoadRecords "db/chiller_bacnet.db", "IFO=H0"
dbLoadRecords "db/fces_bacnet.db", "IFO=H0"


## Set this to see messages from mySub
#var mySubDebug 1

## Run this to trace the stages of iocInit
#traceIocInit

cd ${TOP}/iocBoot/${IOC}
iocInit

drvBACnetStart

## Start any sequence programs
#seq sncExample, "user=8w4Host"

